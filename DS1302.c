#include "DS1302.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#ifndef CEPORT
#define CEPORT PORTC
#define CEBIT PC2
#define CEDDR DDRC
#endif

#ifndef CLKPORT
#define CLKPORT PORTC
#define CLKBIT PC0
#define CLKDDR DDRC
#endif

#ifndef IOPORT
#define IOPORT PORTC
#define IOBIT PC1
#define IODDR DDRC
#define IOPIN PINC
#endif

#define CESet                                                                  \
  { CEPORT |= _BV(CEBIT); }
#define CEClr                                                                  \
  { CEPORT &= ~_BV(CEBIT); }
#define CLKSet                                                                 \
  { CLKPORT |= _BV(CLKBIT); }
#define CLKClr                                                                 \
  { CLKPORT &= ~_BV(CLKBIT); }

#define IOSet                                                                  \
  { IOPORT |= _BV(IOBIT); }
#define IOClr                                                                  \
  { IOPORT &= ~_BV(IOBIT); }
#define IOGet (IOPIN & _BV(IOBIT))
#define IOOut                                                                  \
  { IODDR |= _BV(IOBIT); }
#define IOIn                                                                   \
  { IODDR &= ~_BV(IOBIT); }

void DS1302Init(void) {
  CEDDR |= _BV(CEBIT);
  CLKDDR |= _BV(CLKBIT);
  IOIn;
}

uint8_t BCD2BIN(uint8_t x) { return ((x >> 4) * 10) + (x & 0x0f); }

uint8_t BIN2BCD(uint8_t x) { return (((x / 10) << 4) | (x % 10)); }

void DS1302Write(uint8_t B) {
  IOOut;
  for (uint8_t i = 8; i; i--) {
    if (B & 0x01) {
      IOSet;
    } else {
      IOClr;
    }
    _delay_us(1);
    CLKSet;
    _delay_us(3);
    CLKClr;
    B >>= 1;
  };
}

uint8_t DS1302Read(void) {
  uint8_t B = 0;

  IOIn;
  for (uint8_t i = 8; i; i--) {
    CLKClr;
    B >>= 1;
    _delay_us(1);
    if (IOGet) {
      B |= 0x80;
    }
    CLKSet;
    _delay_us(3);
  };
  CLKClr;
  return (B);
}

void DS1302WriteByte(uint8_t adr, uint8_t data) {
  DS1302Init();
  CESet;
  _delay_us(1);
  adr &= 0x3f;
  adr <<= 1;
  adr |= 0x80;
  DS1302Write(adr);
  DS1302Write(data);
  _delay_us(1);
  CEClr;
}

uint8_t DS1302ReadByte(uint8_t adr) {
  uint8_t A;
  DS1302Init();
  CESet;
  _delay_us(1);
  adr &= 0x3f;
  adr <<= 1;
  adr |= 0x81;
  DS1302Write(adr);
  A = DS1302Read();
  _delay_us(1);
  CEClr;
  return (A);
}

void DS1302ReadTime(struct tm *t) {
  DS1302Init();
  CESet;
  _delay_us(1);
  DS1302Write(0x81 | 0x3E); // BURST MODE!
  t->tm_sec = BCD2BIN(DS1302Read());
  t->tm_min = BCD2BIN(DS1302Read());
  t->tm_hour = BCD2BIN(DS1302Read());
  t->tm_mday = BCD2BIN(DS1302Read());
  t->tm_mon = BCD2BIN(DS1302Read()) - 1;
  t->tm_wday = DS1302Read() - 1;
  t->tm_year = BCD2BIN(DS1302Read());
  _delay_us(1);
  CEClr;
}

void DS1302WriteTime(struct tm *t) {
  DS1302WriteByte(0x07, 0x00); // Enable write
  DS1302WriteByte(0x00, 0x80); // Stop clock
  DS1302WriteByte(0x01, BIN2BCD(t->tm_min));
  DS1302WriteByte(0x02, BIN2BCD(t->tm_hour));
  DS1302WriteByte(0x03, BIN2BCD(t->tm_mday));
  DS1302WriteByte(0x04, BIN2BCD(t->tm_mon + 1));
  DS1302WriteByte(0x05, t->tm_wday + 1); // 1..7 a tm 0..6
  DS1302WriteByte(0x06, BIN2BCD(t->tm_year));
  DS1302WriteByte(0x08, 0xA5);               // Enable charger (1 Diode, 2kΩ)
  DS1302WriteByte(0x00, BIN2BCD(t->tm_sec)); // Write second + start clock
  DS1302WriteByte(0x07, 0x80);               // Disable write
}

void DS1302WriteByteCli(uint8_t adr, uint8_t data) {
  uint8_t _SREG = SREG;
  cli();
  DS1302WriteByte(adr, data);
  SREG = _SREG;
}

uint8_t DS1302ReadByteCli(uint8_t adr) {
  uint8_t _SREG = SREG;
  cli();
  uint8_t tmp = DS1302ReadByte(adr);
  SREG = _SREG;
  return tmp;
}

void DS1302ReadTimeCli(struct tm *t) {
  uint8_t _SREG = SREG;
  cli();
  DS1302ReadTime(t);
  SREG = _SREG;
}

void DS1302WriteTimeCli(struct tm *t) {
  uint8_t _SREG = SREG;
  cli();
  DS1302WriteTime(t);
  SREG = _SREG;
}

time_t DS1302ReadTimeU(void) {
  struct tm t;
  DS1302ReadTime(&t);
  return mktime(&t);
}

void DS1302ReadTimeUpadeU(time_t *t) {
  time_t tmp;
  time_t dtmp;
  dtmp = *t;
  for (uint8_t i = 100; i; i--) {
    tmp = DS1302ReadTimeU();
    dtmp = tmp - dtmp;
    //if ((dtmp >= 0) && (dtmp <= 5)) {
    //time_t je unsigned, nema tedy smysl testovat na vetsi nez 0.
    //Nevadi, i kdyby dtmp melo vyjit mensi nez 0, pokud se zobrazi jako unsigned, bude to vetsi nez 5
    if (dtmp <= 5) {
      // Nacteny cas z DS1302 je vetsi nez predeny cas a zaroven neni vetsi o
      // vyce nez 5 sec.
      *t = tmp;
      return;
    }
    // Nepovedlo se. Udelam kompletni nacteny casu z DS1302, bez ohledu na
    // predanou hodnotu.
    dtmp = DS1302ReadTimeU();
  }
}
