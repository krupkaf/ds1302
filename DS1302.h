#ifndef DS1302_H_
#define DS1302_H_

#include "TimeTools.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void DS1302WriteByte(uint8_t adr, uint8_t data);
uint8_t DS1302ReadByte(uint8_t adr);
void DS1302ReadTime(struct tm *t);
void DS1302WriteTime(struct tm *t);
time_t DS1302ReadTimeU(void);
/**
 * Vertifikovane nacteni casu z DS1302.
 */
void DS1302ReadTimeUpadeU(time_t *t);

void DS1302WriteByteCli(uint8_t adr, uint8_t data);
uint8_t DS1302ReadByteCli(uint8_t adr);
void DS1302ReadTimeCli(struct tm *t);
void DS1302WriteTimeCli(struct tm *t);

#ifdef __cplusplus
}
#endif

#endif
