#include "DS1302.h"
void setup() {
        Serial.begin(115200);
        Serial.setTimeout(1000000);
}

void printtm(struct tm *t) {
        Serial.print(t->tm_year);
        Serial.print('-');
        Serial.print(t->tm_mon);
        Serial.print('-');
        Serial.print(t->tm_mday);
        Serial.print(' ');
        Serial.print(t->tm_hour);
        Serial.print(':');
        Serial.print(t->tm_min);
        Serial.print('-');
        Serial.print(t->tm_sec);
        Serial.print(' ');
        Serial.print(t->tm_wday);
}

void loop() {
        char c;
        Serial.println();
        Serial.println(F(" DS1302 test "));
        Serial.println();
        Serial.print(F("Set RTC [y/n] "));
        Serial.readBytes(&c, 1);
        Serial.println(c);
        if (c == 'y') {
                struct tm t;

                Serial.print(F(" tm_year: "));
                t.tm_year = Serial.parseInt();
                Serial.println(t.tm_year);

                Serial.print(F(" tm_mon: "));
                t.tm_mon = Serial.parseInt();
                Serial.println(t.tm_mon);

                Serial.print(F(" tm_mday: "));
                t.tm_mday = Serial.parseInt();
                Serial.println(t.tm_mday);

                Serial.print(F(" tm_wday: "));
                t.tm_wday = Serial.parseInt();
                Serial.println(t.tm_wday);

                Serial.print(F(" tm_hour: "));
                t.tm_hour = Serial.parseInt();
                Serial.println(t.tm_hour);

                Serial.print(F(" tm_min: "));
                t.tm_min = Serial.parseInt();
                Serial.println(t.tm_min);

                Serial.print(F(" tm_sec: "));
                t.tm_sec = Serial.parseInt();
                Serial.println(t.tm_sec);
                Serial.println();
                printtm(&t);
                Serial.println();
                DS1302WriteTime(&t);
        }


        for(;;) {
                struct tm t;
                DS1302ReadTime(&t);
                printtm(&t);
                Serial.println();
                delay(1000);
        }
}
